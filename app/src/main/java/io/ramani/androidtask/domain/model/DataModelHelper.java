package io.ramani.androidtask.domain.model;

import java.util.List;

public class DataModelHelper {
    private List<DataModel> dataModel;
    private Boolean isLocallyFound;

    public DataModelHelper() {
    }

    public DataModelHelper(List<DataModel> dataModel, Boolean isLocallyFound) {
        this.dataModel = dataModel;
        this.isLocallyFound = isLocallyFound;
    }

    public List<DataModel> getDataModel() {
        return dataModel;
    }

    public void setDataModel(List<DataModel> dataModel) {
        this.dataModel = dataModel;
    }

    public Boolean getLocallyFound() {
        return isLocallyFound;
    }

    public void setLocallyFound(Boolean locallyFound) {
        isLocallyFound = locallyFound;
    }
}
