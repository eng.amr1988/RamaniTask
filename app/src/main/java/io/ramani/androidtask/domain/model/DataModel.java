package io.ramani.androidtask.domain.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

import io.ramani.androidtask.domainCore.IBuilder;

@Entity(tableName = "data_model")
public class DataModel implements Serializable {
  @PrimaryKey(autoGenerate = true)
  @ColumnInfo(name = "id", index = true)
  public Integer localId;

  @ColumnInfo(name = "trackId")
  public int trackId;
  @ColumnInfo(name = "collectionId")
  public int collectionId;
  @ColumnInfo(name = "artistId")
  public int artistId;
  @ColumnInfo(name = "artistName")
  public String artistName;
  @ColumnInfo(name = "collectionName")
  public String collectionName;
  @ColumnInfo(name = "artistViewUrl")
  public String artistWebPage;
  @ColumnInfo(name = "artworkUrl100")
  public String singerPhoto;
  @ColumnInfo(name = "collectionPrice")
  public float collectionPrice;
  @ColumnInfo(name = "trackCount")
  public int trackCount;
  @ColumnInfo(name = "trackNumber")
  public int trackNumber;
  @ColumnInfo(name = "trackPrice")
  public float price;
  @ColumnInfo(name = "currency")
  public String currency;

  @ColumnInfo(name = "trackName")
  public String trackName;

  public DataModel() {
  }

  private DataModel(int paramInt1, int paramInt2, int paramInt3, String paramString1, String paramString2, String paramString3, String paramString4, float paramFloat1, int paramInt4, int paramInt5, float paramFloat2, String paramString5, String paramString6) {
    this.trackId = paramInt1;
    this.collectionId = paramInt2;
    this.artistId = paramInt3;
    this.artistName = paramString1;
    this.collectionName = paramString2;
    this.artistWebPage = paramString3;
    this.singerPhoto = paramString4;
    this.collectionPrice = paramFloat1;
    this.trackCount = paramInt4;
    this.trackNumber = paramInt5;
    this.price = paramFloat2;
    this.currency = paramString5;
    this.trackName = paramString6;
  }

  public static class Builder implements IBuilder<DataModel> {
    private int artistId;

    private String artistName;

    private String artistWebPage;

    private int collectionId;

    private String collectionName;

    private float collectionPrice;

    private String currency;

    private float price;

    private String singerPhoto;

    private int trackCount;

    private int trackId;

    private String trackName;

    private int trackNumber;

    public Builder artistId(int param1Int) {
      this.artistId = param1Int;
      return this;
    }

    public Builder artistName(String param1String) {
      this.artistName = param1String;
      return this;
    }

    public Builder artistWebPage(String param1String) {
      this.artistWebPage = param1String;
      return this;
    }

    public DataModel build() {
      return new DataModel(this.trackId, this.collectionId, this.artistId, this.artistName, this.collectionName, this.artistWebPage, this.singerPhoto, this.collectionPrice, this.trackCount, this.trackNumber, this.price, this.currency, this.trackName);
    }

    public Builder collectionId(int param1Int) {
      this.collectionId = param1Int;
      return this;
    }

    public Builder collectionName(String param1String) {
      this.collectionName = param1String;
      return this;
    }

    public Builder collectionPrice(float param1Float) {
      this.collectionPrice = param1Float;
      return this;
    }

    public Builder currency(String param1String) {
      this.currency = param1String;
      return this;
    }

    public Builder price(float param1Float) {
      this.price = param1Float;
      return this;
    }

    public Builder singerPhoto(String param1String) {
      this.singerPhoto = param1String;
      return this;
    }

    public Builder trackCount(int param1Int) {
      this.trackCount = param1Int;
      return this;
    }

    public Builder trackId(int param1Int) {
      this.trackId = param1Int;
      return this;
    }

    public Builder trackName(String param1String) {
      this.trackName = param1String;
      return this;
    }

    public Builder trackNumber(int param1Int) {
      this.trackNumber = param1Int;
      return this;
    }
  }
}
