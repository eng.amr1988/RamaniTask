package io.ramani.androidtask.domain;

import java.util.List;

import io.ramani.androidtask.data.appleData.DataRepository;
import io.ramani.androidtask.data.common.APIConstants;
import io.ramani.androidtask.domain.model.DataModel;
import io.reactivex.Single;

public class DataUseCase {
    private static DataRepository repo = new DataRepository();

    public static Single<List<DataModel>> loadDataListUseCase(String searchKey) {
        return repo.getData(searchKey, APIConstants.SEARCH_CATEGORY).flatMap(result -> {
            if (!result.isEmpty()) {
                repo.saveData(result);
            }
            return Single.just(result);
        });
    }
}
