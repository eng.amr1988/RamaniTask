package io.ramani.androidtask.data.appleData;

import io.ramani.androidtask.domain.model.DataModel;
import io.reactivex.Single;
import java.util.List;

public interface DataDataSource {
    Single<List<DataModel>> getData(String term, String domainName);

    void saveData(List<DataModel> dataModelList);
}
