package io.ramani.androidtask.data.db;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.ramani.androidtask.domain.model.DataModel;
import java.util.List;

public class DataTypeConverter {
	public static String measurementsToString(List<DataModel> paramList) {
		return (new Gson()).toJson(paramList, (new TypeToken<List<DataModel>>() {

		}).getType());
	}

	public static List<DataModel> stringToData(String paramString) {
		return (List<DataModel>)(new Gson()).fromJson(paramString, (new TypeToken<List<DataModel>>() {

		}).getType());
	}
}
