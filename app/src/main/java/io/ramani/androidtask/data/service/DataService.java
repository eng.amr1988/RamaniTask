package io.ramani.androidtask.data.service;

import io.ramani.androidtask.data.DataAPI;
import io.ramani.androidtask.data.common.APIConstants;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class DataService {

    private static final String URL = APIConstants.BASE_URL;

    private final DataAPI dataApi;

    private static DataService singleton;

    private DataService() {
        Retrofit mRetrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClientWithLoggingInterceptor())
                .baseUrl(URL).build();

        dataApi = mRetrofit.create(DataAPI.class);
    }

    private OkHttpClient httpClientWithLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        return client;
    }

    public static DataService getInstance() {
        if (singleton == null) {
            synchronized (DataService.class) {
                if (singleton == null) {
                    singleton = new DataService();
                }
            }
        }
        return singleton;
    }

    public DataAPI getDataApi() {
        return dataApi;
    }

}
