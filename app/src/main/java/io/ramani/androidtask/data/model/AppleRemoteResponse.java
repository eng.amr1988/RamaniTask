package io.ramani.androidtask.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AppleRemoteResponse {
    @SerializedName("resultCount")
    public int resultCount;

    @SerializedName("results")
    public List<DataRemoteModel> results;


}
