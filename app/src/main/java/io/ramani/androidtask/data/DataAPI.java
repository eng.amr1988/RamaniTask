package io.ramani.androidtask.data;

import io.ramani.androidtask.data.model.AppleRemoteResponse;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface DataAPI {
    @GET("https://itunes.apple.com/search")
    Single<AppleRemoteResponse> getData(@Query("term") String paramString1, @Query("entity") String paramString2);
}
