package io.ramani.androidtask.data.common;

public class APIConstants {
    public static final String BASE_URL = "https://itunes.apple.com";

    public static final String SEARCH_CATEGORY = "musicVideo";
}
