package io.ramani.androidtask.data.appleData;

import java.util.List;

import io.ramani.androidtask.data.DataAPI;
import io.ramani.androidtask.data.service.DataService;
import io.ramani.androidtask.domain.model.DataModel;
import io.reactivex.Single;

public class DataRepository {
  private DataAPI dataAPI;

  private DataLocalDataSource dataLocalDataSource;

  private DataRemoteDataSource dataRemoteDataSource;

  public DataRepository() {
    DataAPI dataAPI = DataService.getInstance().getDataApi();
    this.dataAPI = dataAPI;
    this.dataRemoteDataSource = DataRemoteDataSource.getInstance(dataAPI);
    this.dataLocalDataSource = DataLocalDataSource.getInstance();
  }

  public Single<List<DataModel>> getData(String term, String domainName) {
    return dataLocalDataSource.getData(term, domainName).flatMap(dataList -> {
      if (dataList.isEmpty()) {
        return dataRemoteDataSource.getData(term, domainName);
      } else {
        return Single.just(dataList);
      }
    });
  }

  public void saveData(List<DataModel> paramList) {
    this.dataLocalDataSource.saveData(paramList);
  }
}
