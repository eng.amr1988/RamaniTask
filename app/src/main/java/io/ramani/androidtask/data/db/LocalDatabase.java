package io.ramani.androidtask.data.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import io.ramani.androidtask.BuildConfig;
import io.ramani.androidtask.domain.model.DataModel;

@Database(
        entities = {DataModel.class},
        version = BuildConfig.VERSION_CODE,
        exportSchema = false
)
public abstract class LocalDatabase extends RoomDatabase {
    public abstract DataDao dataDao();
}
