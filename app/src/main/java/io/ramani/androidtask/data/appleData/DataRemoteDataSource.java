package io.ramani.androidtask.data.appleData;

import java.util.List;

import io.ramani.androidtask.data.DataAPI;
import io.ramani.androidtask.data.mappers.DataRemoteMapper;
import io.ramani.androidtask.domain.model.DataModel;
import io.reactivex.Single;

public class DataRemoteDataSource implements DataDataSource {
    private static DataRemoteDataSource instance;

    private final DataAPI dataApi;

    private final DataRemoteMapper dataRemoteMapper = new DataRemoteMapper();

    private DataRemoteDataSource(DataAPI paramDataAPI) {
        this.dataApi = paramDataAPI;
    }

    public static DataRemoteDataSource getInstance(DataAPI paramDataAPI) {
        if (instance == null)
            instance = new DataRemoteDataSource(paramDataAPI);
        return instance;
    }

    public Single<List<DataModel>> getData(String term, String domainName) {
        String str = term;
        if (term.isEmpty())
            str = "popular";
        return this.dataApi.getData(str, domainName).flatMap(data -> {
            return Single.just(dataRemoteMapper.mapFrom(data.results));
        });
    }

    public void saveData(List<DataModel> dataModelList) {
    }
}
