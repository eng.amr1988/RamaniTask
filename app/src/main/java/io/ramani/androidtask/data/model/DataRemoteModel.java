package io.ramani.androidtask.data.model;

import com.google.gson.annotations.SerializedName;

import io.ramani.androidtask.domainCore.IBuilder;

public class DataRemoteModel {
    @SerializedName("trackId")
    public int trackId;
    @SerializedName("collectionId")
    public int collectionId;
    @SerializedName("artistId")
    public int artistId;
    @SerializedName("artistName")
    public String artistName;
    @SerializedName("collectionName")
    public String collectionName;
    @SerializedName("artistViewUrl")
    public String artistWebPage;
    @SerializedName("artworkUrl100")
    public String singerPhoto;
    @SerializedName("collectionPrice")
    public float collectionPrice;
    @SerializedName("trackCount")
    public int trackCount;
    @SerializedName("trackNumber")
    public int trackNumber;
    @SerializedName("trackPrice")
    public float price;
    @SerializedName("currency")
    public String currency;

    @SerializedName("trackName")
    public String trackName;

    public DataRemoteModel() {
        this.trackId = -1;
        this.collectionId = -1;
        this.artistId = -1;
        this.artistName = "";
        this.collectionName = "";
        this.artistWebPage = "";
        this.singerPhoto = "";
        this.collectionPrice = -1;
        this.trackCount = -1;
        this.trackNumber = -1;
        this.price = -1;
        this.currency = "";
        this.trackName = "";
    }

    private DataRemoteModel(int trackId, int collectionId, int artistId, String artistName, String collectionName, String artistWebPage, String singerPhoto, float collectionPrice, int trackCount, int trackNumber, float price, String currency, String trackName) {
        this.trackId = trackId;
        this.collectionId = collectionId;
        this.artistId = artistId;
        this.artistName = artistName;
        this.collectionName = collectionName;
        this.artistWebPage = artistWebPage;
        this.singerPhoto = singerPhoto;
        this.collectionPrice = collectionPrice;
        this.trackCount = trackCount;
        this.trackNumber = trackNumber;
        this.price = price;
        this.currency = currency;
        this.trackName = trackName;
    }

    public static class Builder implements IBuilder<DataRemoteModel> {
        private int trackId;
        private int collectionId;
        private int artistId;
        private String artistName;
        private String collectionName;
        private String artistWebPage;
        private String singerPhoto;
        private float collectionPrice;
        private int trackCount;
        private int trackNumber;
        private float price;
        private String currency;
        private String trackName;

        public Builder trackId(int trackId) {
            this.trackId = trackId;
            return this;
        }

        public Builder collectionId(int collectionId) {
            this.collectionId = collectionId;
            return this;
        }

        public Builder artistId(int artistId) {
            this.artistId = artistId;
            return this;
        }

        public Builder artistName(String artistName) {
            this.artistName = artistName;
            return this;
        }

        public Builder collectionName(String collectionName) {
            this.collectionName = collectionName;
            return this;
        }

        public Builder artistWebPage(String artistWebPage) {
            this.artistWebPage = artistWebPage;
            return this;
        }

        public Builder singerPhoto(String singerPhoto) {
            this.singerPhoto = singerPhoto;
            return this;
        }

        public Builder collectionPrice(float collectionPrice) {
            this.collectionPrice = collectionPrice;
            return this;
        }

        public Builder trackCount(int trackCount) {
            this.trackCount = trackCount;
            return this;
        }

        public Builder trackNumber(int trackNumber) {
            this.trackNumber = trackNumber;
            return this;
        }

        public Builder price(float price) {
            this.price = price;
            return this;
        }

        public Builder currency(String currency) {
            this.currency = currency;
            return this;
        }

        public Builder trackName(String trackName) {
            this.trackName = trackName;
            return this;
        }

        @Override
        public DataRemoteModel build() {
            return new DataRemoteModel(trackId, collectionId, artistId,
                    artistName, collectionName, artistWebPage, singerPhoto,
                    collectionPrice, trackCount, trackNumber,
                    price, currency, trackName);
        }
    }
}
