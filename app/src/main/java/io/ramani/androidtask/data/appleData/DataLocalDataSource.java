package io.ramani.androidtask.data.appleData;

import io.ramani.androidtask.App;
import io.ramani.androidtask.data.db.DataDao;
import io.ramani.androidtask.domain.model.DataModel;
import io.reactivex.Single;
import java.util.List;

public class DataLocalDataSource implements DataDataSource {
    private static DataLocalDataSource instance;

    private DataDao dao;

    public DataLocalDataSource() {
        App.getInstance();
        this.dao = App.getmAppDatabase().dataDao();
    }

    public static DataLocalDataSource getInstance() {
        if (instance == null)
            instance = new DataLocalDataSource();
        return instance;
    }

    public Single<List<DataModel>> getData(String term, String domainName) {
        return term.trim().isEmpty() ? this.dao.getAllData() : this.dao.getAllData(term);
    }

    public void saveData(List<DataModel> dataModelList) {
        DataModel[] arrayOfDataModel = new DataModel[dataModelList.size()];
        dataModelList.toArray(arrayOfDataModel);
        this.dao.insertData(arrayOfDataModel);
    }
}
