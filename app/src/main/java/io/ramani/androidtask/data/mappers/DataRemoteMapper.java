package io.ramani.androidtask.data.mappers;


import java.util.ArrayList;
import java.util.List;

import io.ramani.androidtask.data.model.DataRemoteModel;
import io.ramani.androidtask.domain.model.DataModel;
import io.ramani.androidtask.domainCore.ModelMapper;

public class DataRemoteMapper implements ModelMapper<DataRemoteModel, DataModel> {
    @Override
    public DataModel mapFrom(DataRemoteModel from) {
        return new DataModel.Builder()
                .artistId(from.artistId)
                .trackId(from.trackId)
                .collectionId(from.collectionId)
                .artistName(from.artistName)
                .artistWebPage(from.artistWebPage)
                .collectionName(from.collectionName)
                .collectionPrice(from.collectionPrice)
                .currency(from.currency)
                .price(from.price)
                .singerPhoto(from.singerPhoto)
                .trackCount(from.trackCount)
                .trackNumber(from.trackNumber)
                .trackName(from.trackName)
                .build();
    }

    @Override
    public List<DataModel> mapFrom(List<DataRemoteModel> from) {
        ArrayList dataList = new ArrayList<DataModel>();
        for (int i = 0; i < from.size(); i++) {
            DataModel dataModel = new DataModel.Builder()
                    .artistId(from.get(i).artistId)
                    .trackId(from.get(i).trackId)
                    .collectionId(from.get(i).collectionId)
                    .artistName(from.get(i).artistName)
                    .artistWebPage(from.get(i).artistWebPage)
                    .collectionName(from.get(i).collectionName)
                    .collectionPrice(from.get(i).collectionPrice)
                    .currency(from.get(i).currency)
                    .price(from.get(i).price)
                    .singerPhoto(from.get(i).singerPhoto)
                    .trackCount(from.get(i).trackCount)
                    .trackNumber(from.get(i).trackNumber)
                    .trackName(from.get(i).trackName)
                    .build();

            dataList.add(dataModel);
        }
        return dataList;
    }

    @Override
    public DataRemoteModel mapTo(DataModel to) {
        return new DataRemoteModel.Builder()
                .artistId(to.artistId)
                .trackId(to.trackId)
                .collectionId(to.collectionId)
                .artistName(to.artistName)
                .artistWebPage(to.artistWebPage)
                .collectionName(to.collectionName)
                .collectionPrice(to.collectionPrice)
                .currency(to.currency)
                .price(to.price)
                .singerPhoto(to.singerPhoto)
                .trackCount(to.trackCount)
                .trackNumber(to.trackNumber)
                .trackName(to.trackName)
                .build();
    }

    @Override
    public List<DataRemoteModel> mapTo(List<DataModel> to) {
        ArrayList dataRemoteList = new ArrayList<DataRemoteModel>();
        for (int i = 0; i < to.size(); i++) {
            DataRemoteModel dataRemoteModel = new DataRemoteModel.Builder()
                    .artistId(to.get(i).artistId)
                    .trackId(to.get(i).trackId)
                    .collectionId(to.get(i).collectionId)
                    .artistName(to.get(i).artistName)
                    .artistWebPage(to.get(i).artistWebPage)
                    .collectionName(to.get(i).collectionName)
                    .collectionPrice(to.get(i).collectionPrice)
                    .currency(to.get(i).currency)
                    .price(to.get(i).price)
                    .singerPhoto(to.get(i).singerPhoto)
                    .trackCount(to.get(i).trackCount)
                    .trackNumber(to.get(i).trackNumber)
                    .trackName(to.get(i).trackName)
                    .build();

            dataRemoteList.add(dataRemoteModel);
        }
        return dataRemoteList;
    }
}
