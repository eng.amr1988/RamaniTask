package io.ramani.androidtask.data.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import io.ramani.androidtask.domain.model.DataModel;
import io.reactivex.Completable;
import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface DataDao {
    @Query("select * from data_model")
    Single<List<DataModel>> getAllData();

    @Query("select * from data_model as model where model.artistName like :searchKeyword " +
            " or model.collectionName like :searchKeyword " +
            " or model.artistViewUrl like :searchKeyword " +
            " or model.artworkUrl100 like :searchKeyword " +
            " or model.trackName like :searchKeyword")
    Single<List<DataModel>> getAllData(String searchKeyword);

    @Insert(onConflict = REPLACE)
    Completable insertData(DataModel data);

    @Insert(onConflict = REPLACE)
    void insertData(DataModel... dataModelList);

    @Delete
    Completable delete(DataModel data);
}
