package io.ramani.androidtask.app.ui.base;

import androidx.lifecycle.ViewModel;

public abstract class BaseViewModel extends ViewModel {}
