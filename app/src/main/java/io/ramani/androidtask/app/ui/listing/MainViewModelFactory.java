package io.ramani.androidtask.app.ui.listing;

import androidx.lifecycle.ViewModelProvider;

public class MainViewModelFactory implements ViewModelProvider.Factory {
    public <T extends androidx.lifecycle.ViewModel> T create(Class<T> paramClass) {
        if (paramClass.isAssignableFrom(MainViewModel.class))
            return (T)new MainViewModel();
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
