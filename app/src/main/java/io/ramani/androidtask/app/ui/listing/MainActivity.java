package io.ramani.androidtask.app.ui.listing;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import io.ramani.androidtask.R;
import io.ramani.androidtask.app.ui.base.BaseActivity;
import io.ramani.androidtask.app.ui.details.DetailsActivity;
import io.ramani.androidtask.domain.model.DataModel;

public class MainActivity extends BaseActivity<MainViewModel> implements ListingAdapter.AdapterInteractions {

    private ListingAdapter adapter = new ListingAdapter(new ArrayList());
    private ProgressBar loader;
    private SearchView searchView;

    @NonNull
    @NotNull
    @Override
    protected MainViewModel createViewModel() {
        MainViewModelFactory factory = new MainViewModelFactory();
        return ViewModelProviders.of(this, factory).get(MainViewModel.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        adapter.setAdapterInteractions(this);
        loader = findViewById(R.id.loader);
        searchView = findViewById(R.id.search_view);
        viewModel.loadData("");
        initSubscribers();
        setupRV();
        setupSearchView();
    }

    private void setupSearchView() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                viewModel.loadData(newText);
                return false;
            }
        });
    }

    private void setupRV() {
        RecyclerView dataRV = findViewById(R.id.data_rv);
        dataRV.setAdapter(adapter);
        dataRV.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initSubscribers() {
        subscribeLoadingData();
        subscribeLoaders();
    }

    private void subscribeLoaders() {
        viewModel.loadingVisibleliveData.observe(this, aBoolean -> {
            int view = -1;
            if (aBoolean) {
                view = View.VISIBLE;
            } else {
                view = View.GONE;
            }
            loader.setVisibility(view);
        });
    }

    private void subscribeLoadingData() {
        viewModel.dataliveData.observe(this, dataModelList -> {
            adapter.setNewInstance(dataModelList);
        });
    }

    @Override
    public void onItemClick(DataModel dataModel) {
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra(DetailsActivity.TAG, dataModel);
        startActivity(intent);
    }
}
