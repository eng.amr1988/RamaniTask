package io.ramani.androidtask.app.ui.listing;

import androidx.lifecycle.MutableLiveData;

import java.util.List;

import io.ramani.androidtask.app.ui.base.BaseViewModel;
import io.ramani.androidtask.domain.DataUseCase;
import io.ramani.androidtask.domain.model.DataModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainViewModel extends BaseViewModel {
    private static final String TAG = "MainViewModel";

    MutableLiveData<List<DataModel>> dataliveData = new MutableLiveData();

    MutableLiveData<Boolean> loadingVisibleliveData = new MutableLiveData();

    public void loadData(String paramString) {
        this.loadingVisibleliveData.postValue(Boolean.valueOf(true));
        DataUseCase.loadDataListUseCase(paramString).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(dataModelList -> {
            loadingVisibleliveData.postValue(false);
            dataliveData.postValue(dataModelList);
        }, onError -> {
            loadingVisibleliveData.postValue(false);
        });
    }
}
