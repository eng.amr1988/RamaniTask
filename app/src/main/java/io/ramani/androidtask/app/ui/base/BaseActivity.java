package io.ramani.androidtask.app.ui.base;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

public abstract class BaseActivity<VM extends BaseViewModel> extends AppCompatActivity {
    protected VM viewModel;

    protected abstract VM createViewModel();

    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        this.viewModel = createViewModel();
    }
}
