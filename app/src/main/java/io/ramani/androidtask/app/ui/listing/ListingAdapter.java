package io.ramani.androidtask.app.ui.listing;

import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import io.ramani.androidtask.R;
import io.ramani.androidtask.domain.model.DataModel;

public class ListingAdapter extends BaseQuickAdapter<DataModel, BaseViewHolder> {
    AdapterInteractions adapterInteractions;

    public ListingAdapter(List<DataModel> data) {
        super(R.layout.item_data, data);
    }

    public void setAdapterInteractions(AdapterInteractions adapterInteractions) {
        this.adapterInteractions = adapterInteractions;
    }

    @Override
    protected void convert(@NotNull BaseViewHolder helper, DataModel dataModel) {
        Glide.with(getContext()).load(dataModel.singerPhoto).apply(RequestOptions.circleCropTransform())
                .into((ImageView) helper.getView(R.id.singer_image));

        helper.setText(R.id.artist_name, dataModel.artistName);
        helper.setText(R.id.collection_name, dataModel.collectionName);
        helper.setText(R.id.track_name, dataModel.trackName);
        helper.itemView.setOnClickListener(v -> {
            adapterInteractions.onItemClick(dataModel);
        });
    }

    interface AdapterInteractions {
        void onItemClick(DataModel dataModel);
    }
}
