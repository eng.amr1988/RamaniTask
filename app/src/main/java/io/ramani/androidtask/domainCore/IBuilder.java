package io.ramani.androidtask.domainCore;

public interface IBuilder<T> {
    T build();
}
