package io.ramani.androidtask;

import android.app.Application;

import androidx.room.Room;

import io.ramani.androidtask.data.db.LocalDatabase;

public class App extends Application {

    private static App sInstance;
    private static LocalDatabase mAppDatabase;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        mAppDatabase = Room.databaseBuilder(
                getApplicationContext(),
                LocalDatabase.class, "Android-Task-DB"
        ).fallbackToDestructiveMigration().build();
    }

    public static App getInstance() {
        return sInstance;
    }

    public static LocalDatabase getmAppDatabase() {
        return mAppDatabase;
    }
}
